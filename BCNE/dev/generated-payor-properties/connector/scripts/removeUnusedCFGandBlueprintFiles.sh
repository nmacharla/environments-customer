#SCRIPT_DIR=`getScriptDir $0`
HE_DIR=$1
echo "BCBSNE SH : removeUnusedCFGandBlueprintFiles.sh start"

rm -f $HE_DIR/etc/com.healthedge.customer.bcbsne.*correspondence.*.cfg
rm -f $HE_DIR/etc/com.healthedge.customer.bcbsne.*nacha*.cfg

rm -f $HE_DIR/custom-blueprints-deploy/*BCBSNE*nacha*.xml

echo "BCBSNE SH : removeUnusedCFGandBlueprintFiles.sh end"